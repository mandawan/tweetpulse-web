# Tweetpulse-web
Web app working with [tweetpulse](https://github.com/UzfulLab/tweetpulse-object-python) object.

###Dependencies :
- [MeteorJS](https://www.meteor.com/install)
- [Iron CLI](https://github.com/iron-meteor/iron-cli)

Launch ```iron run``` to run the project.

Replace all ```meteor``` commands by ```iron```

###TODO
* fix little front bugs
* display actual pulse_level when clic on "options"
