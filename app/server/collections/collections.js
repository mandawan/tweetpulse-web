Users = new Mongo.Collection("usersmqtt");
Stats = new Mongo.Collection("statsExpressions");
EmailForTPNews = new Mongo.Collection("emailForTPNews");

// Add the messages in collection of user
Meteor.addMsgToCollection = function (channelId, message, lang) {

    var user = Users.findOne({
        "channelId": channelId
    });

    if (user.expressions) {
        tabMessages = [];
        for (var i = 0; i < user.expressions.length; i++) {
            tabMessages.push(user.expressions[i]);
        };

    }

    tabMessages.push(message);
    tabMessages_active.push(message);
    console.log(tabMessages_active);



    Users.update({
        "channelId": channelId
    }, {
        $push: {
            expressions: {
                "expression": message,
                "lang": lang
            },
            "expressions_active": {
                "expression": message,
                "lang": lang
            }
        }
    });
};
