/*****************************************************************************/
/*  Server Methods */
/*****************************************************************************/

Meteor.methods({
    startClient: function (channelId) {
        mqttClient.subscribe(channelId);
    },

    raz: function (channelId) {
      console.log("channelId raz",channelId);
        Users.update({
            "channelId": channelId
        }, {
            $unset: {
                "expressions_active": []
            }
        })
        tabMessages = [];
        tabMessages_active = [];
    },

    // send the topic query to the caller
  /*  getTopicQuery: function () {
        return topicDefault;
    },

    setTopicQuery: function (newTopic) {
        topicDefault = newTopic;
    },*/

    // publishes a message with a channelId to the broker
    publishMessage: function (channelId, msg, lang) {
      console.log("publish message");
      console.log("channelID to send : ",channelId);
      // TODO change 0 to real number of stats
        mqttClient.publish(channelId, '0:' + msg + ':' + lang, function () {
            console.log("message : ",msg);
            console.log("channelId : ",channelId);

            Meteor.addMsgToCollection(channelId, msg, lang);
        });
    },

    findAllUser: function () {
        return Users.find({}).fetch();
    },

    findOneUserById: function (id) {
        return Users.findOne({
            "_id": id
        });
    },

    findOneUserByIdentify: function (identify) {
        return Users.findOne({
            "identify": identify
        });
    },

    findOneUserByNameId: function (name, identify) {
        return Users.findOne({
            "name": name,
            "identify": identify
        });
    },
    addEmailForNews: function (email) {
      // if(validateEmail(email)){
      //
      //   console.log(("mail valide"));
      // } else {
      //   console.log("email invalide");
      // }
        EmailForTPNews.insert({
            'email': email
        });
    },
    validateEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },

/********
Add new user
fields :
  * name : name user defined
  * identify : first part of MAC adress of object
  * channelId : unique mqtt channel for server communication
  * pulse_level : [numberOfTweet]/[minute],[hour] or [day] default is 1 tweet per hour
  * expressions : all expressions searched by the user
  * expressions_active : actual searched expression

********/
    addUser: function (name, identify) {
        Users.insert({
            'name': name,
            'identify': identify,
            'channelId': 'uzful_data/' + identify,
            'pulse_level': "1/m",
            'expressions': [],
            'expressions_active': []
        });

        Accounts.createUser({
            username: identify,
            password: " "
        });
    },

    //Remove User
    removeUser: function (id) {
        return Users.remove({
            'identify': id
        });
    },

    //Stats
    averageStats: function (stats) {
        var somme = 0;
        var moyenne = 0;

        for (var i = 0; i < stats.length; i++) {
            somme = somme + parseInt(stats[i].stats);
        };

        moyenne = somme / stats.length;

        return Math.round(moyenne);
    },

    getStatsByExpression: function(id, expression){
        var somme = 0;
        var statsExpression;

        var stats = Stats.findOne({
            "userid": id
        });
        console.log("userid getStatsByExpression :", id);

            for(var i=0; i < stats.expressions.length; i++){
              console.log("getStatsByExpression dans boucle");
              console.log(stats.expressions[i].expression);
              if (stats.expressions[i].expression == expression){
                    console.log("rentrer");
                  statsExpression = stats.expressions[i].stats;
              }
        }
        if (statsExpression != undefined) {
          for (var e = 0; e < statsExpression.length; e++) {
                  somme = somme + parseInt(statsExpression[e]);
          }
        } else {
          somme = 0;
        }


        return somme;
    },

    // Get stats by id user and expressions
    getStats: function(expression, id){
        var stats = Stats.find({'_id': "66Q87n6ix9jd6cTkv"}).fetch();
        for (var i = 0; i < stats.length; i++){
            for(var j=0; j < stats[i].expressions.length; j++){
              if (stats[i].expressions[j].expression == expression){
                return stats[i].expressions[j];
              }
            }
        }

        return 0;
    },

    getTweetBefore: function (expression) {
        var date = "2015-08-27"; // change to new Date().format("YYY-MM-DD")
        var all = expression + ' since:' + date;
        T.get('search/tweets', {
                q: all,
                count: 100
            },
            Meteor.bindEnvironment(function (err, data, response) {
                var tabTweetsHbH = [];
                var h = 0;
                var tweets = 0;
                var someString, date;
                while (h != 24) {
                    for (var i = 0; i < data.statuses.length - 1; i++) {
                        someString = JSON.stringify(data.statuses[i].created_at);
                        /*console.log(someString);*/

                        date = someString.replace("+0000 ", "");
                        /*console.log(new Date(date).getHours());*/
                        if (new Date(date).getHours() == h) {
                            tweets++;
                        }
                    }

                    tabTweetsHbH.push({
                        hour: h,
                        tweets: tweets
                    });
                    tweets = 0, someString = 0, date = 0;
                    h++;
                }
                ServerSession.set("tweets", tabTweetsHbH);

            }));
            console.log(ServerSession.get("tweets"));

            return ServerSession.get("tweets");
        },
    definePulseLevel:function(pulseLevel, userId){
      var currentUser = Users.find({
          '_id': userId
      }).fetch();
      console.log("currentUser :", currentUser[0].identify)
      //update user collection
      Users.update({
          '_id': userId
      }, {
          $set: {
              'pulse_level': pulseLevel
          }
      });
      // send to object with mqtt
      mqttClient.publish("uzful_control/"+currentUser[0].identify, "pulse_level:"+pulseLevel );
    },
    pulseIt: function (channelId, intensity){
        mqttClient.publish(channelId, "PULSE/./"+intensity);
    },
    getCurrentExpression: function (id){
      var stats = Stats.findOne({
          "userid": id
      });
      var length = stats.expressions.length;
      return stats.expressions[length - 1].expression;
    },
    getlastExpressions: function (id){
      var stats = Stats.findOne({
          "userid": id
      });
      var length = stats.expressions.length;
      var lastExpressionsArray = [];
      lastExpressionsArray.push(stats.expressions[length - 2].expression);
      lastExpressionsArray.push(stats.expressions[length - 3].expression);
      lastExpressionsArray.push(stats.expressions[length - 4].expression);
      lastExpressionsArray.push(stats.expressions[length - 5].expression);
      lastExpressionsArray.push(stats.expressions[length - 6].expression);
      return lastExpressionsArray;
    },
});

saveStats = Meteor.bindEnvironment(function (message) {
    var today = new Date();
    console.log("message :", message);

    var userID = Users.findOne({
        "channelId": message.channelId
    }, {
        fields: {
            '_id': 1
        }
    });
    console.log("userID : ",userID)
    var statsUser = Stats.find({
        'userid': userID._id
    }).fetch();

    //LIKE SUR REQUETE with regex (new RegExp(search));)

    var separator = ':'; //'/./'
    var arrayOfMessage = message.message.toLocaleString().split(separator);

    var expressionOfMessage = arrayOfMessage[1];
    var statsOfMessage = arrayOfMessage[0];

    var messageWithDate = {date: today, stats: statsOfMessage};

    console.log("statsUser : ", statsUser);


    if (statsUser.length > 0) {
        if (statsUser[0].userid) {

            //Search if expression exist
            var expressionExist = Stats.find({
                'userid': userID._id,
                "expressions.expression": expressionOfMessage
            }).fetch();
            console.log("ex", expressionExist);

            if (expressionExist.length == 0) {
                console.log('expressionExist == null');
                Stats.update({
                    'userid': userID._id
                }, {
                    $push: {
                        expressions: {
                            "expression": expressionOfMessage,
                            "stats": [messageWithDate]
                        }
                    }
                });
            } else {
                console.log('Update stats');
                var statsUpdate = [];

                for (var i = 0; i < statsUser[0].expressions.length; i++) {
                    if (statsUser[0].expressions[i].expression == expressionOfMessage) {
                        console.log(statsUser[0].expressions[i].stats);
                        for (var j = 0; j < statsUser[0].expressions[i].stats.length; j++) {
                            statsUpdate.push(statsUser[0].expressions[i].stats[j]);
                        };
                    };

                };

                statsUpdate.push(messageWithDate);

                console.log(statsUpdate);

                Stats.update({
                    'userid': userID._id,
                    "expressions.expression": expressionOfMessage
                }, {
                    $set: {
                        "expressions.$.stats": statsUpdate
                    }
                });
            }
        } else {
            console.log('Insert new user with stats');
            Stats.insert({
                "userid": userID._id,
                "expressions": [{
                    "expression": expressionOfMessage,
                    'date': today,
                    'stats': [messageWithDate]
                }]
            });
        }
    } else {

        console.log('Insert new user with stats');
        Stats.insert({
            "userid": userID._id,
            "expressions": [{
                "expression": expressionOfMessage,
                'date': today,
                'stats': [messageWithDate]
            }]
        });
    }


    console.log("save");
});
