//Default channelId
var dataTopic = "uzful_data/+";
var controlTopic = "uzful_control/+";
var initTopic = "uzful_init/+";

//Parameter Default
var options = {
    port: 1883,
    host: "web2.uzful.fr"
};

mqttClient = mqtt.connect(options);
var tabMessages = [];
var tabMessages_active = [];

mqttClient
    .on("connect", function () {
        mqttClient.subscribe(dataTopic);
        // mqttClient.subscribe(controlBox);
        mqttClient.subscribe(controlTopic);
        mqttClient.subscribe(initTopic);
        // console.log("mqtt client connected");
        // mqttClient.publish("uzful_test", 'hellooooo');
    })
    .on("message", function (channelId, message) {
        /*var clientId = channelId.slice(8, channelId.length);*/
        var indexSeperator = channelId.indexOf("/");
        var clientId = channelId.slice(indexSeperator + 1, channelId.length);
        console.log("channelId : ",channelId);
        console.log("clientId and message", clientId, message);

        if ("uzful_data/" + clientId == channelId) {
            // build the object to store
            var msg = {
                "channelId": channelId,
                "message": message
            };
            saveStats(msg);
        } else if ("uzful_init/" + clientId == channelId) {
            console.log('sendForInit');
            Meteor.sendForInit(clientId);
        }

    })

.on("error", function (param) {
    console.log("mqtt error");
});

Meteor.sendForInit = Meteor.bindEnvironment(function (clientId) {
    console.log(clientId);
    var user = Users.findOne({
        "identify": clientId
    });
    console.log("user : ",user);

    Meteor.call('definePulseLevel',user.pulse_level, user._id);

    for (var i = 0; i < user.expressions_active.length; i++) {
        var test = false;
        var average = Stats.findOne({
            "userid": user._id,
        });

        for (var j = 0; j < average.expressions.length; j++) {
            if (user.expressions_active[i] == average.expressions[j].expression) {
                Meteor.call("averageStats", average.expressions[j].stats, function (err, response) {
                  console.log("user.channelId : ",user.channelId);
                  console.log(response + ":" + user.expressions_active[i].expression + ":" + user.expressions_active[i].lang);
                    mqttClient.publish(user.channelId, response + ":" + user.expressions_active[i].expression + ":" + user.expressions_active[i].lang);
                });
                test = true;
                j = average.expressions.length;
            }
        }

        if (test == false) {
          console.log("publish expression_active");
          console.log("user.channelId : ",user.channelId);
          console.log("user.expression_active[i].expression : ",user.expressions_active[i].expression);
            mqttClient.publish(user.channelId, "0:" + user.expressions_active[i].expression + ":" + user.expressions_active[i].lang);
        }
    }
});

// Check tendency
Meteor.checkPulse = function(channelId, statRecent, statOlder){
    Meteor.call('growthStats', statRecent, statOlder, function (err, response){
        if(err)
            return err;
        else
            if(response > limit){
                mqttClient.publish(channelId, "PULSE:"+response);
            }
    });
}
