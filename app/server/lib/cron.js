// Remove stats
SyncedCron.add({
    name: 'Remove stats',
    schedule: function (parser) {
        // parser is a later.parse object
        return parser.text('on the first day of the week');
    },
    job: function () {
        var currentDate = new Date();
        /*  Stats.drop();
          console.log('Remove stats');*/

        // Date => MM/DD/YY
        var stats = Stats.find({}).fetch();

        for (var i = 0; i < stats.length; i++) {
            for (var j = 0; j < stats[i].expressions.length; j++) {
                var timeDiff = Math.abs(currentDate.getTime() - stats[i].expressions[j].date.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if (diffDays == 7) {
                    Stats.update({
                        '_id': stats[i]._id
                    }, {
                        $pull: {
                            'expression': stats[i].expressions[j].expression
                        }
                    })
                }
            }
        }


    }
});

//Send Stats
SyncedCron.add({
    name: 'Send average',
    schedule: function (parser) {
        // parser is a later.parse object
        return parser.text('every 5 mins');
    },
    job: function () {
        var allStats = Stats.find({}).fetch();

        for (var i = 0; i < allStats.length; i++) {
            var Findtopic = Users.findOne({
                "_id": allStats[i].userid
            }, {
                fields: {
                    topic: 1,
                    _id: 0
                }
            });
            for (var j = 0; j < allStats[i].expressions.length; j++) {
                Meteor.call("averageStats", allStats[i].expressions[j].stats, function (err, response) {
                    mqttClient.publish(Findtopic.topic, response + ":" + allStats[i].expressions[j].expression);
                });
            };

        };

        // var rand = Math.random() * (110 - 0) + 0;
        // var randArrondir = Math.round(rand);
        // mqttClient.publish("data/A5J6E3", randArrondir.toString()+"/./Apple watch");
        // console.log(randArrondir.toString());
    }
});
