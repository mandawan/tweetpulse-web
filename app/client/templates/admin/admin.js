/*****************************************************************************/
/* Admin: Event Handlers */
/*****************************************************************************/
Template.Admin.events({
    'submit #formUser': function (event, template) {
        var name = event.target.name.value;
        var identify = event.target.identify.value;

        if (name.length > 1) {
            if (isValidIdentify(identify) && testSpecialCharacter(identify) == false) {

                Meteor.call('addUser', name, identify);

                // Clear form
                event.target.name.value = "";
                event.target.identify.value = "";
                location.reload();

            } else {
                template.$("#identify").addClass('error');
            }
        } else {
            template.$("#name").addClass('error');
        }

        // Prevent default form submit
        return false;
    },
    'click #remove_fields': function (event, template) {
        Meteor.call("removeUser", $(event.target).attr("data-id"));
        location.reload();
    },

    'click #identify': function (event, template) {
        template.$("#identify").removeClass('error');
    },

    'click #name': function (event, template) {
        template.$("#name").removeClass('error');
    }
});

/*****************************************************************************/
/* Admin: Helpers */
/*****************************************************************************/
Template.Admin.helpers({
    users: function () {
        Meteor.call('findAllUser', function (err, response) {
            if (err) {
                console.log(err)
            }
            Session.set('data', response);
        });

        return Session.get("data");
    }
});

/*****************************************************************************/
/* Admin: Lifecycle Hooks */
/*****************************************************************************/
Template.Admin.onCreated(function () {
});

Template.Admin.onRendered(function () {
});

Template.Admin.onDestroyed(function () {
});

// Check length
var isValidIdentify = function (val) {
    return val.length == 6 ? true : false;
}

// Check special character
var testSpecialCharacter = function (val) {
    var regex = new RegExp("^[a-zA-Z0-9.]*$");
    return regex.test(val) ? false : true;
}

Template.RegisterTips.events({
    //Open popup
    'click .cd-popup-tipsid': function (event) {
        event.preventDefault();

        $('.cd-popup-idTips').addClass('is-visible');
    },
    //Close popup
    'click .cd-popup-idTips': function (event, template) {
        if ($(event.target).is('.cd-popup-close-idTips') || $(event.target).is('.cd-popup-idTips') ||  $(event.target).is('.cancel-idTips')) {

            event.preventDefault();
            $('.cd-popup-idTips').removeClass('is-visible');

        }
}

});
