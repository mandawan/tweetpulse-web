/*****************************************************************************/
/* AdminLogin: Event Handlers */
/*****************************************************************************/
Template.AdminLogin.events({
    'click .cd-popup, keypress': function (event, template) {
        //Get value
        if ($(event.target).is('#co') || event.keyCode == 13) {

            Meteor.loginWithPassword(template.$("#name")[0].value, template.$("#pwd")[0].value, function (err) {
                if (err) {
                    console.log(err);
                    template.$("#name").addClass('error');
                    template.$("#pwd").addClass('error');
                } else {
                    if (Meteor.user().profile.role == "admin") {
                        Router.go('/admin/' + Meteor.user()._id);
                    } else {
                        //Return 404
                        template.$("#name").addClass('error');
                        template.$("#pwd").addClass('error');
                    }

                }
            });

        } else if ($(event.target).is('#cancel')) {

            event.preventDefault();
            $('.cd-popup').removeClass('is-visible');
            Router.go('/');

        }

    },

    'click #pwd': function (event, template) {
        template.$("#pwd").removeClass('error');
    },

    'click #name': function (event, template) {
        template.$("#name").removeClass('error');
    }
});

/*****************************************************************************/
/* AdminLogin: Helpers */
/*****************************************************************************/
Template.AdminLogin.helpers({
});

/*****************************************************************************/
/* AdminLogin: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminLogin.onCreated(function () {
});

Template.AdminLogin.onRendered(function () {
});

Template.AdminLogin.onDestroyed(function () {
});
