/*****************************************************************************/
/* Dashboard: Event Handlers */
/*****************************************************************************/
Template.Dashboard.events({
    'keyup .message': function (event) {
        /*if((document.getElementsByTagName('input')[0].value) != '' && !document.getElementsByClassName('remove_field')[0]){
            $(".input_field").append('<button class="remove_field">Remove</button>');
        }else if((document.getElementsByTagName('input')[0].value) == '' && document.getElementsByClassName('remove_field')[0]){
            document.getElementsByClassName('remove_field')[0].parentNode.removeChild(document.getElementsByClassName('remove_field')[0]);
        }*/

        /*if (event.keyCode == 13) {
            event.preventDefault();

            if (x < max_fields) {
                //add input box
                $(".input_fields_wrap").append('<div class="input_field"><input type="text" class="message" placeholder="message"/><button class="remove_field">Remove</button></div>');
                x++;
            }
        }*/
    },
    'click .save': function (event) {
        var lang = $("#lang option:selected").val();
        var inputMessage = document.getElementsByClassName('message')[0].value;
        var str = window.location.pathname.split('/');
        var id = str[2];

        Meteor.call('findOneUserById', id, function (err, response) {

            Meteor.call("startClient", response.channelId);
            Meteor.call("raz", response.channelId);

            if(inputMessage != '' && inputMessage != undefined){
              console.log("lang just before publishMessage : ", lang);
                Meteor.call("publishMessage", response.channelId, inputMessage, lang);
                $('.message').val("");
                $('.alert-danger').addClass('hidden');
                $('.alert-success').removeClass('hidden');
                setTimeout(function(){$('.alert').addClass('hidden');},3000);
                Router.go("/dashboard/"+id+"/"+inputMessage);
                window.location.reload();
            } else {
                $('.alert-danger').removeClass('hidden');
            }

        });
    }
});

/*****************************************************************************/
/* Dashboard: Helpers */
/*****************************************************************************/
Template.Dashboard.helpers({
});

/*****************************************************************************/
/* Dashboard: Lifecycle Hooks */
/*****************************************************************************/
Template.Dashboard.onCreated(function () {
  var url = window.location.pathname;
  var userId = url.split("/")[2];
  Meteor.call('getCurrentExpression', userId, function (err, response) {
      $('.current_search_content').html(response);
  });

  Meteor.call('getlastExpressions', userId, function (err, response) {
      $('.history_list_item1').html(response[0]);
      $('.history_list_item2').html(response[1]);
      $('.history_list_item3').html(response[2]);
      $('.history_list_item4').html(response[3]);
      $('.history_list_item5').html(response[4]);
  });
});

Template.Dashboard.onRendered(function () {

});

Template.Dashboard.onDestroyed(function () {
});

// --------------------- COMPONENTS -------------------

Template.DashboardOptions.events({
    //Open popup
    'click .cd-popup-trigger': function (event, template) {
        event.preventDefault();

        template.$('.cd-popup-option').addClass('is-visible');
    },

    //Close popup
    'click .cd-popup': function (event, template) {
        if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ||  $(event.target).is('#cancel') || $(event.target).is('#send')) {

            event.preventDefault();
            template.$('.cd-popup').removeClass('is-visible');

        }
    },
    'click #send': function (e, tmpl){
        var radios = document.getElementsByName('radio');
        var url = window.location.pathname;
        var userId = url.split("/")[2];

        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                switch (radios[i].value) {
                  case "laxiste":
                    // Session.set("setTimeChart", 60000);
                    // Session.set("setNbChart", 5);
                    Meteor.call('definePulseLevel','5/m', userId);
                    break;

                  case "curieux" :
                    // Session.set("setTimeChart", 60000);
                    // Session.set("setNbChart", 1);
                    Meteor.call('definePulseLevel','1/m', userId);
                    break;

                  case "parano":
                    // Session.set("setTimeChart", 3600000);
                    // Session.set("setNbChart", 1);
                    Meteor.call('definePulseLevel','1/h', userId);
                    break;

                  case "perso":
                    var tweet = document.getElementById('pulse-level-tweet').value;
                    var time = document.getElementById('pulse-level-time').value;
                    // Session.set("setTimeChart", 3600000);
                    // Session.set("setNbChart", parseInt(input));
                    Meteor.call('definePulseLevel',tweet+'/'+time, userId);
                    break;

                  default:
                    // Session.set("setTimeChart", 60000);
                    // Session.set("setNbChart", 1);
                    Meteor.call('definePulseLevel','1/m', userId);
                    break;
                }
                break;
            }
        }
    }
});

Template.DashboardCounter.events({
    'click .search': function(e, tmpl){
        Meteor.call("findOneUserByIdentify", Meteor.user().username, function (err, response) {
            $('.cd-popup').removeClass('is-visible');
            console.log(response);
            Router.go("/dashboard/" + response._id);
        });
    }
});

// TODO Set interval for live with database
// TODO GET min and hour
Template.DashboardCounter.helpers({
    expression: function(){
       var url = window.location.pathname;
        var expression = url.split("/")[3];
        console.log(expression);
        return expression;
    },
    tweets: function(){
        var url = window.location.pathname;
        var expressions = url.split("/")[3];
        var userId = url.split("/")[2];
        console.log("expression :",expressions);
        console.log("just before call getStatsByExpression");

        //Change by : Meteor.call("getStatsByExpression", Meteor.userId() , expression, Meteor.bindEnvironment(function(err, result)
        Meteor.call("getStatsByExpression", userId, expressions, Meteor.bindEnvironment(function(err, result){
            if(err) console.log(err);

            console.log(result);
            Session.set("datail", result);
        }));
        // chrono();
        return Session.get("datail");
    }
});

Template.DashboardCounter.rendered = function () {
    $('.num').each(function () {
        $(this).prop('count', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 900,
            step: function (show) {
                $(this).text(Math.ceil(show));
            }
        });
    });
    function expression(){
       var url = window.location.pathname;
        var expression = url.split("/")[3];
        console.log(expression);
        return expression;
    };

};
Template.DashboardCounter.onCreated(function () {
  // window.location.reload();
});

Template.DashboardTips.events({
    //Open popup
    'click .cd-popup-trigger': function (event, template) {
        event.preventDefault();

        template.$('.cd-popup').addClass('is-visible');
    },

    //Close popup
    'click .cd-popup': function (event, template) {
        if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ||  $(event.target).is('#cancel')) {

            event.preventDefault();
            template.$('.cd-popup').removeClass('is-visible');

        }
    }
});
