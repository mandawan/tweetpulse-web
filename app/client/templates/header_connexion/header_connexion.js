/*****************************************************************************/
/* HeaderConnexion: Event Handlers */
/*****************************************************************************/
Template.HeaderConnexion.events({
    'click #logout': function (e, tmpl) {
        Meteor.logout(function (err) {
            if (err) {
                console.log(err);
            }
        });
        Router.go("/");
    },
    'click #HerDashboard': function (e, tmpl) {
        console.log(Meteor.user().username);
        Meteor.call("findOneUserByIdentify", Meteor.user().username, function (err, response) {
            $('.cd-popup').removeClass('is-visible');
            console.log(response);
            Router.go("/dashboard/" + response._id);
        });
    }
});

/*****************************************************************************/
/* HeaderConnexion: Helpers */
/*****************************************************************************/
Template.HeaderConnexion.helpers({
});

/*****************************************************************************/
/* HeaderConnexion: Lifecycle Hooks */
/*****************************************************************************/
Template.HeaderConnexion.onCreated(function () {
});

Template.HeaderConnexion.onRendered(function () {
    // $("#intro h1, #contact h1").fitText(1, { minFontSize: '20px', maxFontSize: '48px' });
    // $("#intro .description h3").fitText(1, { minFontSize: '20px', maxFontSize: '26px' });

});

Template.HeaderConnexion.onDestroyed(function () {
});

Template.HeaderConnexion.username = function () {  
    return Meteor.user().username;
}

Template.RegisterTP.events({
    //Open popup
    'click .cd-popup-trigger': function (event) {
        event.preventDefault();

        $('.cd-popup-login').addClass('is-visible');
        $('#identify').focus();
    },

    //Close popup
    'click .cd-popup-login, keypress': function (event, template) {
        if ($(event.target).is('.cd-popup-close-login') || $(event.target).is('.cd-popup-login') ||  $(event.target).is('.cd-popup-cancel-login')) {

            event.preventDefault();
            $('.cd-popup-login').removeClass('is-visible');

        }

        //Get value
        if ($(event.target).is('#co') || event.keyCode == 13) {

            Meteor.loginWithPassword(template.$("#identify")[0].value, " ", function (err) {
                if (err) {
                    console.log(err);
                    template.$("#identify").addClass('error');
                } else {
                    event.preventDefault();
                    Meteor.call("findOneUserByIdentify", template.$("#identify")[0].value, function (err, response) {
                        $('.cd-popup').removeClass('is-visible');
                        console.log(response);
                        Router.go("/dashboard/" + response._id);
                    });

                }
            });
        }
    },

    'click #identify': function (event, template) {
        template.$("#identify").removeClass('error');
    }
});

Template.RegisterTips.events({
    //Open popup
    'click .cd-popup-tipsid': function (event) {
        event.preventDefault();

        $('.cd-popup-idTips').addClass('is-visible');
    },
    //Close popup
    'click .cd-popup-idTips': function (event, template) {
        if ($(event.target).is('.cd-popup-close-idTips') || $(event.target).is('.cd-popup-idTips') ||  $(event.target).is('.cancel-idTips')) {

            event.preventDefault();
            $('.cd-popup-idTips').removeClass('is-visible');

        }
}

});
