/*****************************************************************************/
/* Home: Event Handlers */
/*****************************************************************************/
Template.Home.events({
  'click .buy-tp-bt': function () {
    $('html,body').animate({
      scrollTop: $("#contact").offset().top},
      'slow');
  },
  'click .hiw-bt': function () {
    $('html,body').animate({
      scrollTop: $("#carousel").offset().top},
      'slow');
  },
  'click .subscribe': function () {
    var inputEmail = document.getElementsByClassName('email')[0].value;
    Meteor.call('addEmailForNews', inputEmail);
    document.getElementsByClassName('email')[0].value = "";
  },

});

/*****************************************************************************/
/* Home: Helpers */
/*****************************************************************************/
Template.Home.helpers({
});

/*****************************************************************************/
/* Home: Lifecycle Hooks */
/*****************************************************************************/
Template.Home.onCreated(function () {
});

Template.Home.onRendered(function () {
});

Template.Home.onDestroyed(function () {
});

/*****************************************************************************/
/* Home: COMPONENTS
/*****************************************************************************/

Template.Carousel.rendered = function () {
    //Slider
    $('body').append('<script type="text/javascript" src="/libraries/initializationCarousel.js"></script>');
}

//TODO Debug mailchimp
/*
Template.SubscribeMailChimp.events({
    'click input, keypress': function (event, template) {
       if ($(event.target).is('.subscribe') || event.keyCode == 13) {
           $(".next").addClass('show');
           var mailChimp = new MailChimp(); //apiKey, { version: '2.0' }

           mailChimp.call('campaigns', 'list', {
                   start: 0,
                   limit: 25
               },
               // Callback beauty in action
               function (error, result) {
                   if (error) {
                       console.error('[MailChimp][Campaigns][List] Error: %o', error);
                   } else {
                       // Do something with your data!
                       console.info('[MailChimp][Campaigns][List]: %o', result);
                   }
               }
           );
       }
    },
    'submit .simform': function (event, template) {
        event.preventDefault;
        return false;
    }
});*/
