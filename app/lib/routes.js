Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});


Router.route('/', {
  name: 'home',
  controller: 'HomeController',
  where: 'client'
});

//Admin
Router.route('/admin', {
    name: 'AdminLogin'
});

Router.route('/admin/:identify', {
    name: 'Admin',
});

var requireLogin = function () {
    if (!Meteor.user()) {
        if (Meteor.loggingIn()) {
            this.render(this.loadingTemplate);
        } else {
            this.render('NotFound');
        }
    } else {
        this.next();
    }
}

var requireLoginAdmin = function () {
    if (!Meteor.user()) {
        if (Meteor.loggingIn() && Meteor.user().profile.role == 'admin') {
            this.render(this.loadingTemplate);
        } else {
            this.render('NotFound');
        }
    } else {
        this.next();
    }
}

Router.onBeforeAction(requireLoginAdmin, {
    only: 'admin'
});
Router.onBeforeAction(requireLogin, {
    only: 'dashboard'
});

Router.onBeforeAction('dataNotFound', {
    only: 'dashboard'
});

//Dashboard
Router.route('/dashboard/:_id', {
    name: 'Dashboard',
    data: function () {
        var id = this.params._id;
        console.log("this.params.id", id);

        Meteor.call('findOneUserById', id, function (err, response) {
            Session.set('userId', response);
        });
        return Session.get('user');
    }
    // waitOn: function(){
    //     return [IRLibLoader.load("/libraries/classie.js"), IRLibLoader.load("/libraries/selectFx.js")]
    // }
});


// name:'chart' => Graph
Router.route('/dashboard/:_id/:expression', {
    name: 'DashboardCounter'
});
